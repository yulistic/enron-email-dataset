Author: Jongyul Kim  
Date: 2019-02-02

The repository for a Enron email dataset and its analysis.

Directories:
* `dataset`: Includes preprocessed email datasets. Its subdirectories represent policies of the dataset processed by `tools/generate_dataset.py`. Each subdirectory includes followings:
  * `json.tar`: Includes json formatted mail dataset.
  * `serialized`: A file that contains all the serialized mail dataset. Related script file: `tools/serialize_to_file.py`.
  * `users`: Includes the user lists extracted from the dataset. For example, `xxxxxx` is extracted from `xxxxxx@domain.com`.  
    * `to_internal`: user list from `To`, `Cc`, and `Bcc` lists. These users are internal users which means their domain address is `enron.com`.
    * `to_external`: user list from `To`, `Cc`, and `Bcc` lists. These users are internal users which means their domain address is `enron.com`.
    * `to_total`: `to_internal` + `to_external`
    * `from`: user list gatherd from `From` lists.
    * `total`: `to_total` + `from`  
    * `invalid`: The list of usernames paresed from the Enron dataset but failed to be created as a Linux user. This list is generated from the execution of `tools/add_users.py`.
    * `to_total_filtered`: The list of usernames after filtering out invalid usernames by `filter_users.sh`.
    * `to_total_filtered_out`: The list of invalid usernames. They were filtered out by `filter_users.sh`.
    * `newusers_list`: Input file for the `newusers` command.
* `domains`: Analysis of domains in the dataset.

Files:
* `enron_mail_20150507.tar.gz`: Enron email dataset. It is untar to `maildir` directory.

The Enron email dataset is from the following link.
* https://www.cs.cmu.edu/~./enron/
