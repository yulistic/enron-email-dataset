#!/bin/bash
# pw_name:pw_passwd:pw_uid:pw_gid:pw_gecos:pw_dir:pw_shell

GROUP_NAME="mailwkldusers"
USER_PW="assisenice"
USER_ADD_LIST_FILE="newusers_list"
# USER_SRC_FILE="user_test"
USER_SRC_FILE="to_total_filtered"
HOME_DIR="/home/enron_users"

sudo mkdir -p $HOME_DIR
sudo groupadd $GROUP_NAME
rm -rf $USER_ADD_LIST_FILE
uid=70001   # It must be larger than the current largest value. Check /etc/passwd file.
while read p; do
    #echo "${p}:${USER_PW}:${uid}:${GROUP_NAME}:Postfix users:/home/enron_users/${p}:/bin/bash" >> $USER_ADD_LIST_FILE
    echo "${p}::${uid}:${GROUP_NAME}:Postfix users:${HOME_DIR}/${p}:/bin/bash" >> $USER_ADD_LIST_FILE
    ((uid++))
done < $USER_SRC_FILE

chmod 0600 $USER_ADD_LIST_FILE
sudo newusers $USER_ADD_LIST_FILE
