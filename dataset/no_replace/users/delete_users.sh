#!/bin/bash
# Delete users listed in USER_SRC_FILE.

GROUP_NAME="mailwkldusers"
USER_SRC_FILE="to_total_filtered"

while read p; do
    sudo userdel $p
done < $USER_SRC_FILE

sudo groupdel $GROUP_NAME
